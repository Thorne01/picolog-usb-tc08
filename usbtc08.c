/*******************************************************************\

  Name:         usbtc08.c
  Created by:   Jacob Thorne 

  Contents:     Library file for all the functions needed to run
  				USB-TC08
  				
  Date:			09.12.2023
\********************************************************************/
#include "usbtc08b.h"
#include "usbtc08.h"
#include <stdint.h>

int usb_tc08_connect(char* error)
{
	int16_t serial_port = usb_tc08_open_unit();
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char)); //used to clear the memory, just incase
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	if(serial_port == -1)
	{
		//printf("%i\n", usb_tc08_get_last_error(0));
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(0), error_msg)) //zero is only required here for the get_last_error
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_CONNECT_FAILURE; //if we get an error code that is not not known it returns failure so we pass this
		}
		sprintf(error, "Failed connect: %s", error_msg); //reads error code and passes it up
		return USB_TC08_CONNECT_FAILURE;
	}
	else if(serial_port == 0)
	{
		sprintf(error, "No unit found"); //if its not connected we get this
		return USB_TC08_CONNECT_FAILURE;
	}
	return serial_port;
}

int usb_tc08_disconnect(const int16_t serial_port, char* error)
{
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	if(!usb_tc08_close_unit(serial_port))//most usb_tc08 functions return 1 if success and 0 for fail
	{
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(serial_port), error_msg))//check that serial port works for get_last_error
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_FAILURE;
		}
		sprintf(error, "Failed disconnect: %s", error_msg);
		return USB_TC08_FAILURE;
	}
	return USB_TC08_SUCCESS;
}

int usb_tc08_set_stop(const int16_t serial_port, char* error)
{
//stop running of the device, used for stream mode	
	if(!usb_tc08_stop(serial_port))
	{
		sprintf(error, "Failed set stop");
		return USB_TC08_FAILURE;
	}
	return USB_TC08_SUCCESS;
}

int usb_tc08_set_mains_filter(const int16_t serial_port, int16_t filter_sixty, char* error)
{
//set the mains power frequency to filter out, 0 = 50Hz, 1 = 60Hz
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	if(!usb_tc08_set_mains(serial_port, filter_sixty))
	{
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(serial_port), error_msg))
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_FAILURE;
		}
		sprintf(error, "Failed set mains: %s", error_msg);
		return USB_TC08_FAILURE;
	}
	return USB_TC08_SUCCESS;
}

int usb_tc08_get_minimum_interval(const int16_t serial_port, int32_t* milliseconds, char* error)
{
//has to be called after the set channel function, otherwise will return an error
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	
	int32_t reply = usb_tc08_get_minimum_interval_ms(serial_port);
	//max single channel is 10 samples per second. Absolute minimum sampling interval with all 9 channels is 900 ms.
	if(reply == 0)
	{
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(serial_port), error_msg))
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_FAILURE;
		}
		sprintf(error, "Failed get minimum interval: %s", error_msg);
		return USB_TC08_FAILURE;
	}
	*milliseconds = reply; //passes the minimum interval up
	return USB_TC08_SUCCESS;
}

int usb_tc08_get_info(const int16_t serial_port, char* unit_info, char* error)
{
//asks the device for its information, prints into terminal in format with \n after each item
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));

	if(!usb_tc08_get_formatted_info(serial_port, unit_info, USBTC08_MAX_INFO_CHARS))
	{
		sprintf(error, "Failed get info: Too many bytes to copy");
		return USB_TC08_FAILURE;
	}
	return USB_TC08_SUCCESS;
}

int usb_tc08_set_channel_active(const int16_t serial_port, int16_t channel, char tc_type, char* error)
{
//sets which channels are active
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));

	if(!usb_tc08_set_channel(serial_port, channel, tc_type))
	{
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(serial_port), error_msg))
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_FAILURE;
		}
		sprintf(error, "Failed set channel: %s", error_msg);
		return USB_TC08_FAILURE;
	}
	return USB_TC08_SUCCESS;
}

int usb_tc08_set_run(const int16_t serial_port, int32_t interval, int32_t* allowed_interval, char* error)
{
//setting for interval in given period can be given by the get function
//this depends on the setup of the TC08
//the function will return actual interval allowed by the driver
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	
	int32_t val = usb_tc08_run(serial_port, interval);
	
	if(val ==0)
	{
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(serial_port), error_msg))
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_FAILURE;
		}
		sprintf(error, "Failed set run: %s", error_msg);
		return USB_TC08_FAILURE;
	}
	*allowed_interval = val;
	return USB_TC08_SUCCESS;
}

int usb_tc08_measure_all(const int16_t serial_port, float* temp, int16_t* overflow_flags, int16_t units, char* error)
{
//this function measures all the channels once, returns NaN if channel not active
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	
	if(!usb_tc08_get_single(serial_port, temp, overflow_flags, units))
	{
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(serial_port), error_msg))
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_FAILURE;
		}
		sprintf(error, "Failed measure all: %s", error_msg);
		return USB_TC08_FAILURE;
	}
	return USB_TC08_SUCCESS;
}

int usb_tc08_measure_stream_mode(const int16_t serial_port, float* temp_buffer, int32_t* times_ms_buffer, int32_t buffer_length, int16_t* overflow, int16_t channel, int16_t units, char* error)
{
//measures in stream mode, this means it continiously reads over set time interval
	char error_msg[USB_TC08_MSG_SIZE];
	memset(error, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	memset(error_msg, '\0', USB_TC08_MSG_SIZE* sizeof(char));
	
	int32_t no_of_readings = usb_tc08_get_temp(serial_port, temp_buffer, times_ms_buffer, buffer_length, overflow, channel, units, 0);
	//last option fills missing data with NaNs for 0, 1 fills with previous reading
	//no_of_readings are also the number copied into an array, currently don't make use of this
	if(no_of_readings == -1)
	{
		if(usb_tc08_get_error_status(usb_tc08_get_last_error(serial_port), error_msg))
		{
			sprintf(error, "Failure to find error code");
			return USB_TC08_FAILURE;
		}
		sprintf(error, "Failed measure stream: %s", error_msg);
		return USB_TC08_FAILURE;
	}
	else if(no_of_readings == 0)
	{
		sprintf(error, "Currently no readings to collect");
		return USB_TC08_FAILURE;
	}	
	return USB_TC08_SUCCESS;
}

int usb_tc08_get_error_status(int16_t error_code, char* err_msg)
{
	switch(error_code)
	{
		case -1 :
			sprintf(err_msg, "INVALID HANDLE"); //no error occurred
			break;
		case 0 :
			sprintf(err_msg, "USBTC08_ERROR_OK"); //no error occurred
			break;
		case 1 :
			sprintf(err_msg, "USBTC08_ERROR_OS_NOT_SUPPORTED"); //driver not supports in this OS
			break;
		case 2 :
			sprintf(err_msg, "USBTC08_ERROR_NO_CHANNELS_SET"); //a call to set channels is required
			break;
		case 3 :
			sprintf(err_msg, "USBTC08_ERROR_INVALID_PARAMETER"); //one or more function arguments were invalid
			break;
		case 4 :
			sprintf(err_msg, "USBTC08_ERROR_VARIANT_NOT_SUPPORTED"); //hardware version is not supported
			break;
		case 5 :
			sprintf(err_msg, "USBTC08_ERROR_INCORRECT_MODE"); //incompatible mix of legacy and non-legacy functions was called (i.e. read single in stream mode)
			break;
		case 6 :
			sprintf(err_msg, "USBTC08_ERROR_ENUMERATION_INCOMPLETE"); //open unit in async mode called while enumeration was already in progress
			break;
		case 7 :
			sprintf(err_msg, "USBTC08_ERROR_NOT_RESPONDING"); //cannot get a reply from USB TC-08
			break;
		case 8 :
			sprintf(err_msg, "USBTC08_ERROR_FW_FAIL"); //unable to download firmware
			break;
		case 9 :
			sprintf(err_msg, "USBTC08_ERROR_CONFIG_FAIL"); //missing or corrupted EEPROM
			break;
		case 10 :
			sprintf(err_msg, "USBTC08_ERROR_NOT_FOUND"); //cannot find enumerated deivce
			break;		
		case 11 :
			sprintf(err_msg, "USBTC08_ERROR_THREAD_FAIL"); //threading function failed
			break;
		case 12 :
			sprintf(err_msg, "USBTC08_ERROR_PIPE_INFO_FAIL"); //can not get USB pipe information
			break;
		case 13 :
			sprintf(err_msg, "USBTC08_ERROR_NOT_CALIBRATED"); //no calibration date was found
			break;		
		case 14 :
			sprintf(err_msg, "USBTC08_ERROR_PICOPP_TOO_OLD"); //an old picopp.sys driver was found on the system
			break;
		case 15 :
			sprintf(err_msg, "USBTC08_ERROR_COMMUNICATION"); //the PC has lost communication with the device
			break;
		default :
			return USB_TC08_FAILURE;
    }
    return USB_TC08_SUCCESS;
}
