/*******************************************************************\

  Name:         usbtc08b.h
  Created by:   Jacob Thorne	

  Contents:     Header file for functions defined in the library

  Date:			09.12.2023
\********************************************************************/
#include <stdio.h>
#include <stdint.h>
#include "usbtc08.h"
#include "usbtc08b.h"

int32_t _kbhit() //function to stop the contuinous running of the code in terminal with use of enter
{
    struct termios oldt, newt;
    int32_t bytesWaiting;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    setbuf(stdin, NULL);
    ioctl(STDIN_FILENO, FIONREAD, &bytesWaiting);

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    return bytesWaiting;
}

int8_t stream_mode = 0;

int32_t main(void)
{
	char usb_tc08_error[USB_TC08_MSG_SIZE];
	
	int16_t fd = usb_tc08_connect(usb_tc08_error);
	if(fd == -1){printf("%s\n",usb_tc08_error); return -1;}
	
	char unit_info[256];
	if(usb_tc08_get_info(fd, unit_info, usb_tc08_error)){printf("%s\n",usb_tc08_error); return -1;}
	printf("%s\n", unit_info);
	
	if(usb_tc08_set_mains_filter(fd, 0, usb_tc08_error)){printf("%s\n",usb_tc08_error); return -1;}
	
	for(int8_t i = 0; i < 9; i++)//loop over all channels
	{
		if(usb_tc08_set_channel_active(fd, i, 'K', usb_tc08_error)){printf("%s\n",usb_tc08_error); return -1;}
	}

	if(stream_mode == 0)
    {
		float temp[9] = {0};
		int16_t overflow_flags = 0;
		printf("Entering single read mode\n");
		printf("CJC    Ch1    Ch2    Ch3    Ch4    Ch5    Ch6    Ch7    Ch8\n");
	
		while(!_kbhit()) 
		{
			if(usb_tc08_measure_all(fd, temp, &overflow_flags, 0, usb_tc08_error)){printf("%s\n", usb_tc08_error);return -1;}
			for(int8_t channel = 0; channel < USBTC08_MAX_CHANNELS +1; channel++)
			{
				if(overflow_flags &(1 << channel))//Bitwise comparison to determine the overflow state of each channel.
				{
					printf("\nChannel %d overflowed", channel);
				}
				else{printf("%f\t", temp[channel]);}
			}
			printf("\n");
			sleep(1);
    	}
    }
    
    else{
    		
		int32_t min_interval = 0;
		if(usb_tc08_get_minimum_interval(fd, &min_interval, usb_tc08_error)){printf("%s\n", usb_tc08_error);return -1;}//find minimum interval
	
		int32_t allowed_interval = 0;
		if(usb_tc08_set_run(fd, min_interval, &allowed_interval, usb_tc08_error)){printf("%s\n", usb_tc08_error);return -1;}//sets the TC08 ready for stream mode
    	sleep(1); //required wait for setup of the stream mode
    
		float temp_buffer[64]; //store data for upto a minute (1 second readout)
		int32_t buffer_length = 256; //define buffer length
		int16_t overflow_stream[9] = {0}; //define the overflow flags for each channel
		printf("Entering stream mode\n");
		printf("CJC    Ch1    Ch2    Ch3    Ch4    Ch5    Ch6    Ch7    Ch8\n");
		while(!_kbhit()) 
		{
			for (int8_t channel = 0; channel < USBTC08_MAX_CHANNELS + 1; channel++)
			{
				if(usb_tc08_measure_stream_mode(fd, temp_buffer, NULL, buffer_length, overflow_stream, channel, 0, usb_tc08_error)){printf("%s\n", usb_tc08_error);return -1;}
				printf("%f\t", temp_buffer[0]);
			}
			printf("\n");
			sleep(1);
		}
	}
	
	if(usb_tc08_set_stop(fd, usb_tc08_error)){printf("%s\n", usb_tc08_error);return -1;}

	if(usb_tc08_disconnect(fd, usb_tc08_error)){printf("%s\n", usb_tc08_error);return -1;}
}
