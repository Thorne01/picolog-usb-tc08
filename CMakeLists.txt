CMAKE_MINIMUM_REQUIRED(VERSION 3.10)
project(tc08)

set(CMAKE_CXX_STANDARD 17)

include_directories(/opt/picoscope/include/libusbtc08)
link_directories(/opt/picoscope/lib)

add_library(usbtc08b usbtc08.c)

add_executable(usbtc08Ex usbtc08Ex.c)
target_link_libraries(usbtc08Ex usbtc08b usbtc08 m)
