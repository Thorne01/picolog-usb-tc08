/*******************************************************************\

  Name:         usbtc08Ex.c
  Created by:   Jacob Thorne	

  Contents:     Example code to read out temperature of the USB-TC08
  				in single read or stream mode

  Date:			09.12.2023
\********************************************************************/
#ifndef __usbtc08__
#define __usbtc08__

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include "usbtc08.h"
#include "time.h"
#include <stdint.h>

#define USB_TC08_SUCCESS 0
#define USB_TC08_FAILURE 1
#define USB_TC08_CONNECT_FAILURE -1
#define USB_TC08_MSG_SIZE 512
#define USB_TC08_BUFFER_SIZE 1000

#define CENTIGRADE 0
#define FAHRENHEIT 1
#define KELVIN 2
#define RANKINE 3

int usb_tc08_connect(char* error);
int usb_tc08_get_error_status(int16_t error_code, char* err_msg);
int usb_tc08_disconnect(const int16_t serial_port, char* error);
int usb_tc08_set_stop(const int16_t serial_port, char* error);
int usb_tc08_set_mains_filter(const int16_t serial_port, int16_t filter_sixty, char* error);
int usb_tc08_get_minimum_interval(const int16_t serial_port, int32_t* milliseconds, char* error);
int usb_tc08_get_info(const int16_t serial_port, char* unit_info, char* error);
int usb_tc08_set_channel_active(const int16_t serial_port, int16_t channel, char tc_type, char* error);
int usb_tc08_set_run(const int16_t serial_port, int32_t interval, int32_t* allowed_interval, char* error);
int usb_tc08_measure_all(const int16_t serial_port, float* temp, int16_t* overflow_flags, int16_t units, char* error);
int usb_tc08_measure_stream_mode(const int16_t serial_port, float* temp_buffer, int32_t* times_ms_buffer, int32_t buffer_length, int16_t* overflow, int16_t channel, int16_t units, char* error);
#endif
